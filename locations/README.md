## Barovia

![barovia](../assets/images/locations/WhatsApp%20Image%202024-05-14%20at%2015.08.19.jpeg)

## Crossroads

![crossroads](../assets/images/locations/WhatsAppVideo2024-04-06at13.35.03-ezgif.com-video-to-gif-converter.gif)

## Gates of Barovia

![barovia gates img](../assets/images/locations/WhatsApp%20Image%202024-05-14%20at%2015.08.19(1).jpeg)
![barovia gates gif](../assets/images/locations/Gates-of-Barovia.gif)

## Tserpool Encampment

![tserpool encampment map](../assets/images/locations/WhatsApp%20Image%202024-05-14%20at%2015.08.20(3).jpeg)
![tserpool encampment gif](../assets/images/locations/TserPoolEncampment.gif)

## Tserpool Falls

![tserpool falls](../assets/images/locations/tser-falls.gif)

## Bonegrinder Mill

![bonegrinder mill](../assets/images/locations/Old-Bonegrinder.gif)
![bonegrinder megaliths](../assets/images/locations/Old-bonegrinder-megaliths.gif)