# Curse of Strahd

## Party

![lidraud](./assets/images/players/WhatsApp%20Image%202024-05-01%20at%2020.05.01.jpeg)

## Players

### Bellin

![bellin](./assets/images/players/WhatsApp%20Image%202024-05-14%20at%2015.08.17(1).jpeg)

### Paraad

![paraad](./assets/images/players/WhatsApp%20Image%202024-05-14%20at%2015.08.17.jpeg)

### Trukagul

![trukagul](./assets/images/players/WhatsApp%20Image%202024-05-14%20at%2015.08.19(6).jpeg)

### Valdmir

![valdmir](./assets/images/players/WhatsApp%20Image%202024-05-14%20at%2016.51.04.jpeg)

### Valthrun

![valthrun](./assets/images/players/WhatsApp%20Image%202024-05-14%20at%2015.08.18.jpeg)
